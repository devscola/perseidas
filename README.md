La naturaleza del repositorio Perseidas es la recopilación de material para impartir talleres sobre tecnologías de servidores.


# Gitlab-pages del proyecto

https://devscola.gitlab.io/perseidas/


# Estructura del proyecto

Repositorio en gitlab: https://gitlab.com/devscola/perseidas.git

Trello: https://trello.com/b/W31tJrld/perseidas-talleres-servidores

Canal Slack: devscola.slack.com , canal #perseidas


# Talleres

- Primer contacto con las tecnologías de servidores.
