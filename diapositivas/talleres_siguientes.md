Taller 2

Orquestación: automatización de la instalación y mantenimiento de infrastructura.

Temas
Scripts (link de command line)
Fully Automated Install
Ansible


Taller 3

Sistemas de aislamientos en servidores: máquinas virtuales y contenedores

Temas
Docker
Vagrant


Taller 4

Integración contínua.
Fases de automatización: development, testing, staging, production.

Temas
Jenkins
Gitlab


Taller 5

Servicios web

Temas
Tipos de proxy y aplicación.
Servicios web: apache2 y nginx
Bases de datos: relaciones y no relacionales


Otros talleres...

Redes:
Hardware de red: módem, router, switch
Ámbitos y tipos de red.
IPv4
IPv6
Servicios de DNS dinámicos.
Crear un mapa de red y actualizarlo al instalar servidores.
